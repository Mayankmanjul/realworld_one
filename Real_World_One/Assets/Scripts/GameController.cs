﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
namespace Real_World_Test
{
    public class GameController : MonoBehaviour
    {
        private static GameController mInstnce;

        public static GameController _Instance {
            get {
                return mInstnce;
            }
        }

        public GameSettings _GameSettings;
        public Transform _CustomerParent;
        public Customer _PfCustomer;


        
        void Awake()
        {
            if (null != mInstnce)
            {
                Debug.LogError("Multiple Instaces, Destroying one");
                Destroy(gameObject);
            }
            else
            {
                mInstnce = this;
                DontDestroyOnLoad(gameObject);
            }
        }
 
        void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
               Customer cust = LeanPool.Spawn(_PfCustomer, Vector3.zero, Quaternion.identity, _CustomerParent);
                cust.Init();
            }
        }
    }

    public enum VegType
    {
        a,
        b,
        c,
        d,
        e,
        f
    }
}
