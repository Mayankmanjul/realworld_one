﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Real_World_Test
{
    public class Customer : MonoBehaviour
    {
        public Image _TimerBar;
        public TMP_Text _SaladText;
        public List<VegType> _SaladCombination;

        float mWaitTime;
        float mMaxWaitTime;
        GameController mGameController;
        List<int> mVegIndex;

        public void Init()
        {
            mGameController = GameController._Instance;
            mMaxWaitTime = mWaitTime = GetWaitTime();
            SetSaladCombo();
        }

        int GetComboLength()// made this method so that in future if logic changes to get the length we can do it here
        {
            return Random.Range(mGameController._GameSettings._MinVegCombination, mGameController._GameSettings._MaxVegCombination + 1);//since second parameter is excluded thats why +1
        }

        float GetWaitTime()// made this method so that in future if logic changes to get the length we can do it here
        {
            return Random.Range(mGameController._GameSettings._CustMinWaitTime, mGameController._GameSettings._CustMaxWaitTime);
        }

        void SetSaladCombo()
        {
            _SaladCombination = new List<VegType>();
            int length = GetComboLength();
            mVegIndex = new List<int>();
            for (int i = 0; i < length; i++)
            {
                GetUniqueRandom();
            }

            for (int i = 0; i < mVegIndex.Count; i++)
            {
                _SaladCombination.Add((VegType)mVegIndex[i]);  
            }
            AssignSaladText();
        }

        void GetUniqueRandom()
        {
            int rand = Random.Range(0, System.Enum.GetNames(typeof(VegType)).Length);
            if (!mVegIndex.Contains(rand))
            {
                mVegIndex.Add(rand);
            }
            else
            {
                GetUniqueRandom();
            }
        }

        void MaintainTimer()
        {
            if (mWaitTime > 0)
            {
                mWaitTime -= Time.deltaTime;
                _TimerBar.fillAmount = NormalizeTimer();
            }
            else
            {
                Lean.Pool.LeanPool.Despawn(this);
            }

        }

        float NormalizeTimer()
        {
            return mWaitTime / mMaxWaitTime;
        }

        void AssignSaladText()
        {
            string text = "";
            for (int i = 0; i < _SaladCombination.Count; i++)
            {
                text += _SaladCombination[i];
                if (i != _SaladCombination.Count - 1)
                    text += ",";
            }
            _SaladText.text = text;
        }

        private void Update()
        {
            MaintainTimer();
        }



    }
}
