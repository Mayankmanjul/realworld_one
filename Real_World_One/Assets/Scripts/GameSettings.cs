﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class GameSettings : ScriptableObject
{
    #region Instance and asset creation
    private static GameSettings mInstance;
    public static GameSettings _Instance
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = Resources.Load("ProductSettings") as GameSettings;
                if (mInstance == null)
                {
                    mInstance = CreateInstance<GameSettings>();
#if UNITY_EDITOR
                    if (!Directory.Exists(Application.dataPath + "/Resources"))
                        AssetDatabase.CreateFolder("Assets", "Resources");

                    AssetDatabase.CreateAsset(mInstance, "Assets/Resources/GameSettings.asset");
                    AssetDatabase.SaveAssets();
#endif
                }
            }

            return mInstance;
        }
    }


#if UNITY_EDITOR
    [MenuItem("RWT_Tools/Configure/GameSettings")]
    public static void CreateProductSettings()
    {
        Selection.activeObject = _Instance;
    }

    public static GameSettings GoToProductSettingsAsset()
    {
        string fullPath = Path.Combine("Assets/Resources", "GameSettings.asset");

        GameSettings instance = AssetDatabase.LoadAssetAtPath(fullPath, typeof(GameSettings)) as GameSettings;

        if (instance == null)
            Debug.LogError(" ProductSettings not found at path : " + fullPath);

        return instance;
    }

    [MenuItem("RWT_Tools/Show Settings")]
    public static void Edit()
    {
        Selection.activeObject = GoToProductSettingsAsset();

        ShowInspector();
    }

    private static void ShowInspector()
    {
        try
        {
            var editorAsm = typeof(UnityEditor.Editor).Assembly;
            var type = editorAsm.GetType("UnityEditor.InspectorWindow");
            UnityEngine.Object[] findObjectsOfTypeAll = Resources.FindObjectsOfTypeAll(type);

            if (findObjectsOfTypeAll.Length > 0)
            {
                ((EditorWindow)findObjectsOfTypeAll[0]).Focus();
            }
            else
            {
                EditorWindow.GetWindow(type);
            }
        }
        catch
        {
        }
    }


#endif
    #endregion

    public int _MinVegCombination;
    public int _MaxVegCombination;
    [Space(10)]
    public float _CustMinWaitTime;
    public float _CustMaxWaitTime;
}
